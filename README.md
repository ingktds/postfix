# Docker image for Postfix

## Build Docker image

```
$ cd [WORK_DIR]/postfix
$ docker build -t <container_image_name> .
```

## Run Docker

```
$ cd [WORK_DIR]/postfix
$ docker run --rm -d -p 25:25 --name postfix <container_image_name>
```
