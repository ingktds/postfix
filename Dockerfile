FROM centos:latest
LABEL maintainer="ingktds <tadashi.1027@gmail.com>"

RUN yum -y install gcc make libdb-devel postfix && \
sed -i 's/inet_interfaces = localhost/#inet_interfaces = localhost/g' /etc/postfix/main.cf && \
curl -LO ftp://ftp.riken.jp/net/postfix/postfix-release/official/postfix-3.3.1.tar.gz && \
tar xzf postfix-3.3.1.tar.gz && \
cd postfix-3.3.1 && \
make && \
make upgrade

EXPOSE 25
CMD [ "/usr/sbin/postfix", "start-fg" ]
